-- Usable Workshop for Payday 2
-- Author:  The Joker  (http://steamcommunity.com/id/thejokerhahaha)
-- Version:  BETA v0.1 - Initial release  (Mostly tested, but let the world beat it up)

local io_open = io.open
local pairs = pairs
local table_insert = table.insert
local table_sort = table.sort
local tonumber = tonumber
local tostring = tostring
local type = type

local DB = DB
local Idstring = Idstring
local MenuCallbackHandler = MenuCallbackHandler
local Steam = Steam
local SystemInfo = SystemInfo

local Hooks = Hooks
local json = json
local LocalizationManager = LocalizationManager
local LuaModManager = LuaModManager
local MenuHelper = MenuHelper
local Net = LuaNetworking
local QuickMenu = QuickMenu
local Utils = Utils

local M_blackmarket
local M_localization
local M_network
local M_skin_editor
local M_workshop

local tweak_data = tweak_data
local T_B_weapon_skins = tweak_data.blackmarket.weapon_skins
local T_E_qualities = tweak_data.economy.qualities
local T_E_rarities = tweak_data.economy.rarities

local data_sep = '-'
local folder_workshop = WorkshopManager.PATH
local idstring_texture = Idstring('texture')

local uw_bundle_folder = 'cash/workshop'
local uw_lang_folder = 'loc/'
local uw_menu_id = 'UW_menu'
local uw_modpath = ModPath
local uw_network = 'UWData'
local uw_rarity = 'workshop'
local uw_save_file = SavePath..'usable_workshop'

-- Implement Workshop rarity
T_E_rarities[uw_rarity] = {
	index = 6,
	color = Color('66C0F4'),
	bg_texture = 'guis/dlcs/cash/textures/pd2/blackmarket/icons/rarity_'..uw_rarity,
	name_id = 'bm_menu_rarity_'..uw_rarity,
}

local add_workshop_skins = {}
local build_menu
local settings_data
local unique_ids = {}
UW_notifications = UW_notifications or {ignore = {}, new = {}, old = {}}
local UW_notifications = UW_notifications

-- Removes complex characters and simplifies text
local function clean_name(name)
	return name:lower():gsub("%W+", "_")
end

-- Returns Workshop ID or weapon ID + author + name
local function get_skin_id(skin)
	local skin_config = skin._config
	if skin_config.workshop_id then
		return skin_config.workshop_id
	end
	local skin_id = skin_config.data.weapon_id..'_'
	if skin_config.author then
		skin_id = skin_id..clean_name(skin_config.author)..'_'
	end
	return skin_id..clean_name(skin_config.name)
end

-- Returns a non-duplicate instance ID for a skin
local function get_unique_instance_id(input)
	local unique = input
	local inc = 0
	while unique_ids[unique] do
		inc = inc + 1
		unique = input..inc
	end
	return unique
end

-- Creates unique ID weapon skin
local function create_workshop_skin(skin, localization)
	local skin_id = get_skin_id(skin)
	if not add_workshop_skins[skin_id] then
		-- Force load textures
		M_skin_editor:add_literal_paths(skin)
		M_skin_editor:load_textures(skin)

		-- Load custom Blackmarket icon
		local textures = M_skin_editor:get_texture_list(skin)
		local icon_name
		for _, texture in pairs(textures) do
			local dot_index = texture:find('.[^.]*$')
			if dot_index and dot_index > 4 and texture:sub(dot_index - 4, dot_index - 1) == 'icon' then
				icon_name = texture:sub(dot_index - 4, -1)
				break
			end
		end
		if icon_name then
			DB:create_entry(idstring_texture, Idstring('guis/dlcs/'..uw_bundle_folder..'/weapon_skins/'..skin_id), M_skin_editor:get_texture_string(skin, icon_name))
		end

		-- Clone Workshop item data to weapon skin
		local skin_config = skin._config
		local skin_data = skin_config.data
		local new_tweak_data = {}
		for k, v in pairs(skin_data) do
			new_tweak_data[k] = v
		end

		-- Add additional weapon skin data
		local name_id = 'bm_wskn_'..skin_id
		new_tweak_data.name_id = name_id
		localization[name_id] = skin_config.name

		local desc_id = name_id..'_desc'
		new_tweak_data.desc_id = desc_id
		localization[desc_id] = M_localization:text('uw_text_workshop_skin')..(skin_config.author and "\n  -  "..skin_config.author or "").."\n\n"..skin._item_path:sub(skin._item_path:find(folder_workshop) or 1)

		if skin_data.weapon_id == 'saw' or skin_data.weapon_id == 'saw_secondary' then
			new_tweak_data.weapon_ids = {'saw', 'saw_secondary'}
		end
		new_tweak_data.rarity = uw_rarity
		new_tweak_data.texture_bundle_folder = uw_bundle_folder

		new_tweak_data.author = skin_config.author
		new_tweak_data.workshop_id = skin_config.workshop_id

		T_B_weapon_skins[skin_id] = new_tweak_data
		local instance_id = get_unique_instance_id(skin_config.timestamp)
		unique_ids[instance_id] = true
		add_workshop_skins[skin_id] = instance_id
		settings_data.qualities[skin_id] = settings_data.qualities[skin_id] or 'mint'
	end
end

-- Remove existing weapon skin
local function remove_workshop_skin(skin)
	local skin_id = get_skin_id(skin)
	if add_workshop_skins[skin_id] then
		T_B_weapon_skins[skin_id] = nil
		unique_ids[add_workshop_skins[skin_id]] = nil
		add_workshop_skins[skin_id] = nil
	end
end

-- Verify and clean weapons in inventory
local function cleanup_workshop_skins()
	local crafted_items = M_blackmarket._global.crafted_items
	for _, crafted_category in pairs({crafted_items.primaries, crafted_items.secondaries}) do
		for _, item in pairs(crafted_category) do
			local skin_id = item.cosmetics and item.cosmetics.id
			if skin_id then
				if T_B_weapon_skins[skin_id] then
					if add_workshop_skins[skin_id] then
						item.cosmetics.quality = settings_data.qualities[skin_id]
					end
				else
					item.cosmetics = nil
				end
			end
		end
	end

	build_menu()
end

-- Save and load settings file
local function save_settings()
	local file = io_open(uw_save_file, 'w')
	if file then
		local old_ignored_names = settings_data.ignored_names
		settings_data.ignored_names = {}
		for workshop_id, skin_name in pairs(old_ignored_names) do
			if settings_data.ignore[workshop_id] then
				settings_data.ignored_names[workshop_id] = skin_name
			end
		end

		local old_qualities = settings_data.qualities
		settings_data.qualities = {}
		for skin_id, quality in pairs(old_qualities) do
			if add_workshop_skins[skin_id] then
				settings_data.qualities[skin_id] = quality
			end
		end

		file:write(json.encode(settings_data))
		file:close()
	end

	cleanup_workshop_skins()
end

local function load_settings()
	local file = io_open(uw_save_file, 'r')
	if file then
		settings_data = json.decode(file:read('*all'))
	end

	settings_data = settings_data or {notifications = true}
	settings_data.ignore = settings_data.ignore or {}
	settings_data.ignored_names = settings_data.ignored_names or {}
	settings_data.qualities = settings_data.qualities or {}
end

-- BLT driven function to just get translation file's abbreviation
local function active_language()
	return LuaModManager._languages[LuaModManager:GetLanguageIndex()] or LuaModManager._languages[LuaModManager:GetIndexOfDefaultLanguage()]
end


-- Initializer
Hooks:PreHook(WorkshopManager, '_init_items', 'WorkshopManager__init_items_UW', function(self)
	for _, item in pairs(self:items() or {}) do
		remove_workshop_skin(item)
	end
end)

Hooks:PostHook(WorkshopManager, '_init_items', 'WorkshopManager__init_items_UW', function(self)
	if not M_blackmarket then
		local managers = managers
		M_blackmarket = managers.blackmarket
		M_localization = managers.localization
		M_network = managers.network
		M_skin_editor = M_blackmarket._skin_editor
		M_workshop = self

		-- Add rarity background
		DB:create_entry(idstring_texture, Idstring(T_E_rarities[uw_rarity].bg_texture), uw_modpath..'rarity_'..uw_rarity..'.texture')

		-- Send Workshop skins to other players and bypass crash from Overkill not checking a skin's existence before indexing it
		local cosmetics_backup
		Hooks:PreHook(BaseNetworkSession, 'check_send_outfit', 'BaseNetworkSession_check_send_outfit_UW', function(_, peer)
			if Net:IsMultiplayer() then
				cosmetics_backup = {}
				for weapon_type, weapon in pairs({primary = M_blackmarket:equipped_primary(), secondary = M_blackmarket:equipped_secondary()}) do
					local skin_id = weapon.cosmetics and weapon.cosmetics.id
					local skin = skin_id and add_workshop_skins[skin_id] and T_B_weapon_skins[skin_id]
					if skin_id and skin then
						local send = weapon_type..data_sep..skin_id..data_sep..weapon.cosmetics.quality..data_sep
						if settings_data.notifications then
							send = send..M_localization:text(skin.name_id)
						end
						if peer then
							Net:SendToPeer(peer, uw_network, send)
						else
							Net:SendToPeers(uw_network, send)
						end
						cosmetics_backup[weapon_type] = weapon.cosmetics
						weapon.cosmetics = nil
					end
				end
			end
		end)

		Hooks:PostHook(BaseNetworkSession, 'check_send_outfit', 'BaseNetworkSession_check_send_outfit_UW', function()
			if Net:IsMultiplayer() then
				for weapon_type, weapon in pairs({primary = M_blackmarket:equipped_primary(), secondary = M_blackmarket:equipped_secondary()}) do
					if cosmetics_backup[weapon_type] then
						weapon.cosmetics = cosmetics_backup[weapon_type]
					end
				end
			end
		end)

		Hooks:PreHook(ConnectionNetworkHandler, 'sync_outfit', 'ConnectionNetworkHandler_sync_outfit_UW', function(self, outfit_string, outfit_version, outfit_signature, sender)
			local peer = self._verify_sender(sender)
			if peer then
				peer.active_workshop_skins = peer.workshop_skins
				peer.workshop_skins = nil
			end
		end)

		local orig_NetworkPeer_blackmarket_outfit = NetworkPeer.blackmarket_outfit
		function NetworkPeer:blackmarket_outfit()
			local outfit = orig_NetworkPeer_blackmarket_outfit(self)
			if self.active_workshop_skins then
				for weapon_type, cosmetics in pairs(self.active_workshop_skins) do
					outfit[weapon_type].cosmetics = cosmetics
				end
			end
			return outfit
		end

		-- Stupid simple localization loading
		LocalizationManager:load_localization_file(uw_modpath..uw_lang_folder..active_language())
	end

	-- Wipe and add skins in "workshop" folder
	local localization = {}
	for _, item in pairs(self:items()) do
		create_workshop_skin(item, localization)
	end
	LocalizationManager:add_localized_strings(localization)

	save_settings()
end)


-- Notifications system
local notification_pop, notification_next, notification_ignore_player, notification_ignore_skin, notification_steam
local last_notification
-- Display notification in the queue
notification_pop = function(allow_next)
	local data = UW_notifications.new[#UW_notifications.new]
	if data and (allow_next or not last_notification or not last_notification.visible) then
		local name, skin_id, skin_name = data[1], data[2], data[3]
		if settings_data.notifications and not (add_workshop_skins[skin_id] or UW_notifications.old[skin_id] or settings_data.ignore[skin_id] or UW_notifications.ignore[name]) then
			last_notification = QuickMenu:new(M_localization:text('uw_text_workshop_skin'), name.." - "..skin_name, {
				{data = skin_id, callback = notification_steam, text = M_localization:text('uw_notification_steam'), is_focused_button = true},
				{callback = notification_next, text = M_localization:text('uw_notification_next')},
				{data = name, callback = notification_ignore_player, text = M_localization:text('uw_notification_ignore')..": "..name},
				{data = {skin_id, skin_name}, callback = notification_ignore_skin, text = M_localization:text('uw_notification_ignore')..": "..skin_name},
				{text = M_localization:text('uw_notification_close'), is_cancel_button = true}
			}, true)
		else
			notification_next()
		end
	end
end

-- Throw out current and pop next
notification_next = function()
	UW_notifications.new[#UW_notifications.new] = nil
	notification_pop(true)
end

-- Ignore player until reset and pop next
notification_ignore_player = function(name)
	UW_notifications.ignore[name] = true
	notification_next()
end

-- Ignore skin permanately and pop next
notification_ignore_skin = function(data)
	settings_data.ignore[data[1]] = true
	settings_data.ignored_names[data[1]] = data[2]
	notification_next()
end

-- Open skin on Steam Workshop in Steam Overlay and re-pop current
notification_steam = function(workshop_id)
	Steam:overlay_activate('url', 'http://steamcommunity.com/sharedfiles/filedetails/?id='..workshop_id)
	notification_pop(true)
end

-- Digest custom message of Workshop skin
local function digest_workshop_message(message)
	local data = {}
	local sep_index
	for i = 1, 3 do
		sep_index = message:find(data_sep)
		if sep_index and sep_index > 1 then
			data[i] = message:sub(1, sep_index - 1)
			message = message:sub(sep_index + 1)
		else
			break
		end
	end
	if sep_index and sep_index > 1 and message:len() > 0 then
		data[4] = message
	end
end
local allowed_weapon_types = {primary = true, secondary = true}
Hooks:Add('NetworkReceivedData', 'NetworkReceivedData_UW', function(sender, id, message)
	if id == uw_network and message and message:len() > 3 then
		local peer = M_network:session():peer(sender)
		if peer then
			local data = digest_workshop_message(message)
			local weapon_type, skin_id, quality, skin_name = data[1], data[2], data[3], data[4]
			if allowed_weapon_types[weapon_type] and skin_id and T_E_qualities[quality] then
				peer.workshop_skins = peer.workshop_skins or {}
				peer.workshop_skins[weapon_type] = add_workshop_skins[skin_id] and {id = skin_id, quality = quality} or nil
				if settings_data.notifications and skin_name and tonumber(skin_id) then
					local name = peer:name()
					if name and not (add_workshop_skins[skin_id] or UW_notifications.old[skin_id] or settings_data.ignore[skin_id] or UW_notifications.ignore[name]) then
						UW_notifications.old[skin_id] = true
						UW_notifications.new[#UW_notifications.new] = {name, skin_id, skin_name}
						if not (Utils:IsInHeist() or Utils:IsInLoadingState()) then
							notification_pop()
						end
					end
				end
			end
		end
	end
end)


-- Fix removal of weapons with invalid skins to just remove skin
Hooks:PreHook(BlackMarketManager, '_cleanup_blackmarket', 'BlackMarketManager__cleanup_blackmarket_UW', function()
	cleanup_workshop_skins()
end)


-- Force Steam lists to accept Workshop skins
Hooks:PreHook(BlackMarketManager, 'tradable_update', 'BlackMarketManager_tradable_update_UW', function(self, tradable_list)
	for skin_id, instance_id in pairs(add_workshop_skins) do
		table_insert(tradable_list, {instance_id = instance_id, category = 'weapon_skins', entry = skin_id, quality = settings_data.qualities[skin_id], amount = 1})
	end
end)

Hooks:PreHook(BlackMarketManager, 'tradable_verify', 'BlackMarketManager_tradable_verify_UW', function(self, category, entry, quality, bonus, tradable_list)
	if category == 'weapon_skins' and T_B_weapon_skins[entry] and T_B_weapon_skins[entry].rarity == uw_rarity and bonus ~= true then
		table_insert(tradable_list, {category = category, entry = entry, quality = quality, bonus = bonus})
	end
end)


-- Added Skin Editor options
local function insert_menu_input(node, params, index)
	local data_node = {
		type = 'MenuItemInput'
	}
	local new_item = node:create_item(data_node, params)
	new_item:set_enabled(params.enabled)
	if index then
		node:insert_item(new_item, index)
	else
		node:add_item(new_item)
	end
	return new_item
end
Hooks:PostHook(MenuSkinEditorInitiator, 'modify_node', 'MenuSkinEditorInitiator_modify_node_UW', function(self, node, data)
	local name = node:parameters().name
	if name == 'skin_editor' then
		local author_input, workshop_id_input
		if not node:item('author') then
			author_input = insert_menu_input(node, {
				enabled = true,
				name = 'author',
				text_id = 'uw_skin_editor_author',
				callback = 'edit_workshop_config_UW'
			}, 12)
			workshop_id_input = insert_menu_input(node, {
				enabled = true,
				name = 'workshop_id',
				text_id = 'uw_skin_editor_workshop_id',
				callback = 'edit_workshop_config_UW'
			}, 13)
		end
		local skin_config = M_skin_editor and M_skin_editor:get_current_skin()
		skin_config = skin_config and skin_config._config
		author_input = author_input or node:item('author')
		workshop_id_input = workshop_id_input or node:item('workshop_id')
		author_input:set_input_text(skin_config and skin_config.author or "")
		workshop_id_input:set_input_text(skin_config and skin_config.workshop_id or "")
		local enabled = skin_config ~= nil
		author_input:set_enabled(enabled)
		workshop_id_input:set_enabled(enabled)
	end
end)
function MenuCallbackHandler:edit_workshop_config_UW(item)
	local skin_config = M_skin_editor:get_current_skin()
	skin_config = skin_config._config
	if skin_config then
		local value = item:value()
		skin_config[item:name()] = value and value:len() > 0 and value or nil
	end
end


-- Update skins according to Workshop editing
Hooks:PreHook(SkinEditor, 'delete_current', 'SkinEditor_delete_current_UW', function(self)
	local skin = self:get_current_skin()
	if skin then
		remove_workshop_skin(skin)
		cleanup_workshop_skins()
	end
end)

Hooks:PreHook(SkinEditor, 'save_skin', 'SkinEditor_save_skin_UW', function(_, skin)
	if skin._config.data then
		remove_workshop_skin(skin)
	end
end)

Hooks:PostHook(SkinEditor, 'save_skin', 'SkinEditor_save_skin_UW', function(_, skin)
	if skin._config.data.name_id then
		local localization = {}
		create_workshop_skin(skin, localization)
		LocalizationManager:add_localized_strings(localization)
		cleanup_workshop_skins()
	end
end)


-- Add custom tags
local orig_SkinEditor_get_current_weapon_tags = SkinEditor.get_current_weapon_tags
function SkinEditor:get_current_weapon_tags()
	local tags = orig_SkinEditor_get_current_weapon_tags(self)
	local skin_config = M_skin_editor:get_current_skin()
	skin_config = skin_config._config
	if skin_config and skin_config.workshop_id then
		table_insert(tags, "Usable Workshop")
	end
	return tags
end


-- Option menu with ability to rebuild itself
Hooks:Add('MenuManagerSetupCustomMenus', 'MenuManagerSetupCustomMenus_UW', function()
	MenuHelper:NewMenu(uw_menu_id)
end)

local _nodes
Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenus_UW', function(_, nodes)
	_nodes = nodes
	nodes[uw_menu_id] = MenuHelper:BuildMenu(uw_menu_id, {back_callback = save_settings})
	MenuHelper:AddMenuItem(MenuHelper:GetMenu('lua_mod_options_menu'), uw_menu_id, 'uw_title', 'uw_desc')
end)

local function sort_items(a, b)
	return a[1] < b[1]
end
local quality_items = {}
local value_to_quality = {}
for quality_id, quality in pairs(T_E_qualities) do
	quality_items[quality.index] = quality.name_id
	value_to_quality[quality.index] = quality_id
end
build_menu = function()
	MenuHelper:NewMenu(uw_menu_id)
	local priority = 0
	local function prio()
		priority = priority - 1
		return priority
	end

	MenuHelper:AddButton({id = 'reload', title = 'uw_options_reload', desc = 'uw_options_reload_desc', callback = 'reload_UW', menu_id = uw_menu_id, priority = prio()})
	MenuHelper:AddToggle({id = 'notifications', title = 'uw_options_notifications', desc = 'uw_options_notifications_desc', value = settings_data.notifications, callback = 'toggle_settings_data_UW', menu_id = uw_menu_id, priority = prio()})
	MenuHelper:AddDivider({size = 16, menu_id = uw_menu_id, priority = prio()})

	local sorted_list = {}
	for skin_id in pairs(add_workshop_skins) do
		table_insert(sorted_list, {M_localization:text(T_B_weapon_skins[skin_id].name_id)..(T_B_weapon_skins[skin_id].workshop_id and " - "..T_B_weapon_skins[skin_id].workshop_id or ""), skin_id})
	end
	if #sorted_list > 0 then
		table_sort(sorted_list, sort_items)
		MenuHelper:AddButton({title = 'uw_options_quality_label', menu_id = uw_menu_id, priority = prio()})
		for _, data in pairs(sorted_list) do
			MenuHelper:AddMultipleChoice({items = quality_items, id = data[2], title = data[1], desc = 'uw_options_quality_desc', callback = 'change_quality_UW', localized = false, menu_id = uw_menu_id, priority = prio(), value = T_E_qualities[settings_data.qualities[data[2]]].index})
		end
		MenuHelper:AddDivider({size = 16, menu_id = uw_menu_id, priority = prio()})
	end

	sorted_list = {}
	for workshop_id, skin_name in pairs(settings_data.ignored_names) do
		table_insert(sorted_list, {skin_name.." - "..workshop_id, workshop_id})
	end
	if #sorted_list > 0 then
		table_sort(sorted_list, sort_items)
		MenuHelper:AddButton({title = 'uw_options_ignored_label', menu_id = uw_menu_id, priority = prio()})
		for _, data in pairs(sorted_list) do
			MenuHelper:AddToggle({id = data[2], title = data[1], desc = 'uw_options_ignored_desc', callback = 'change_ignored_UW', localized = false, menu_id = uw_menu_id, value = true, priority = prio()})
		end
	end
	_nodes[uw_menu_id] = MenuHelper:BuildMenu(uw_menu_id, {back_callback = save_settings})
end
function MenuCallbackHandler:reload_UW()
	M_workshop:_init_items()
	QuickMenu:new(M_localization:text('uw_options_reload'), M_localization:text('uw_notification_reload'), {}, true)
end
local change_off_warnings = {
	notifications = {'uw_options_notifications', 'uw_options_notifications_warning'}
}
function MenuCallbackHandler:toggle_settings_data_UW(item)
	local name = item:name()
	settings_data[name] = item:value() == 'on' or nil
	local warning
	if not settings_data[name] then
		warning = change_off_warnings[name]
	end
	if warning then
		QuickMenu:new(M_localization:text('uw_options_warning')..": "..M_localization:text(warning[1]), M_localization:text(warning[2]), {}, true)
	end
end
function MenuCallbackHandler:change_quality_UW(item)
	settings_data.qualities[item:name()] = value_to_quality[item:value()]
end
function MenuCallbackHandler:change_ignored_UW(item)
	settings_data.ignore[item:name()] = item:value() == 'on' or nil
end

load_settings()