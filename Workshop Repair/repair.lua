Hooks:PreHook(BlackMarketManager, '_cleanup_blackmarket', 'BlackMarketManager__cleanup_blackmarket_Workshop_Repair', function(self)
	local T_B_weapon_skins = tweak_data.blackmarket.weapon_skins
	local crafted_items = self._global.crafted_items
	for _, crafted_category in pairs({crafted_items.primaries, crafted_items.secondaries}) do
		for _, item in pairs(crafted_category) do
			local skin_id = item.cosmetics and item.cosmetics.id
			if skin_id and not T_B_weapon_skins[skin_id] then
				item.cosmetics = nil
			end
		end
	end
end)