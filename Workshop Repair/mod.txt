{
	"name" : "Workshop Repair",
	"description" : "Only use this if instructed to from the Usable Workshop page, otherwise REMOVE THIS!",
	"author" : "The Joker",
	"contact" : "http://steamcommunity.com/id/thejokerhahaha",
	"version" : "1.0",
	"hooks" : [
		{
			"hook_id" : "lib/managers/blackmarketmanager",
			"script_path" : "repair.lua"
		}
	]
}